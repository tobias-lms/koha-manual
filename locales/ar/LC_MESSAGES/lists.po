# Compendium of ar.
msgid ""
msgstr ""
"Project-Id-Version: compendium-ar\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-15 19:31-0300\n"
"PO-Revision-Date: 2018-05-15 19:48-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/lists.rst:4
msgid "Lists & Cart"
msgstr ""

#: ../../source/lists.rst:6
msgid ""
"Lists are a way to save a collection of content on a specific topic or for a "
"specific purpose. The Cart is a session specific storage space."
msgstr ""
"القوائم وسيلة لتوفير مجموعة من المحتوى على موضوع محدد أو لغرض محدد. السلة هي "
"جلسة تخزين محددة."

#: ../../source/lists.rst:9
#, fuzzy
msgid "*Get there:* More > Lists"
msgstr "الوصول إلى هناك: "

#: ../../source/lists.rst:14
msgid "Lists"
msgstr "قوائم"

#: ../../source/lists.rst:19
msgid "Create a List"
msgstr "إنشاء القائمة"

#: ../../source/lists.rst:21
msgid ""
"A list can be created by visiting the Lists page and clicking 'New List'"
msgstr "يمكن إنشاء القئمة عن طريق زيارة صفحة القوائم و النقر على 'قائمة جديدة'"

#: ../../source/lists.rst:23
msgid "|image868|"
msgstr ""

#: ../../source/lists.rst:25
msgid "The new list form offers several options for creating your list:"
msgstr "شكل القائمة الجديدة يقدم خيارات عدة لإنشاء قائمتك:"

#: ../../source/lists.rst:27
msgid "|image869|"
msgstr ""

#: ../../source/lists.rst:29
msgid "The name is what will appear on the list of Lists"
msgstr "الاسم هو ماسوف يظهر في قائمة القوائم"

#: ../../source/lists.rst:31
msgid "You can also choose how to sort the list"
msgstr "يمكنك أيضا اختيار كيفية ترتيب القائمة"

#: ../../source/lists.rst:33
msgid "Next decide if your list is going to be private or public"
msgstr ""

#: ../../source/lists.rst:35
msgid ""
"A Private List is managed by you and can be seen only by you (depending on "
"your permissions settings below)"
msgstr "يتم إدارة القائمة الخاصة من قبلك، والإطلاع عليها فقط من قبلك"

#: ../../source/lists.rst:38
msgid ""
"A Public List can be seen by everybody, but managed only by you (depending "
"on your permissions settings below)"
msgstr ""
"ويمكن الاطلاع على قائمة العامة من قبل الجميع، ولكن يتم إدارتها فقط من قبلك "

#: ../../source/lists.rst:41
msgid ""
"Finally decide what your permissions will be on the list. You can allow or "
"disallow:"
msgstr ""

#: ../../source/lists.rst:44
msgid "anyone else to add entries"
msgstr "أي شخص آخر لإضافة مدخلات."

#: ../../source/lists.rst:46
msgid "anyone to remove his own contributed entries"
msgstr "أي أحد لإزالة مدخلات المساهمين الخاصة به."

#: ../../source/lists.rst:48
#, fuzzy
msgid "**Note**"
msgstr "ملاحظة"

#: ../../source/lists.rst:50
msgid ""
"The owner of a list is always allowed to add entries, but needs permission "
"to remove."
msgstr "مسموح لمالك القائمة دائما بإضافة مدخلات، لكنه يحتاج صلاحية للحذف."

#: ../../source/lists.rst:53
#, fuzzy
msgid "anyone to remove other contributed entries"
msgstr "اي شخص لحذف مدخلات مساهمة أخرى."

#: ../../source/lists.rst:55
msgid "A list can also be created from the catalog search results"
msgstr "يمكن إنشاء قائمة أيضا من نتائج البحث في الفهرس"

#: ../../source/lists.rst:57
msgid "|image870|"
msgstr ""

#: ../../source/lists.rst:59
msgid "Check the box to the left of the titles you want to add to the new list"
msgstr ""
"ضع علامة في المربع إلى اليمين من العناوين التي تريد إضافتها إلى القائمة "
"الجديدة"

#: ../../source/lists.rst:62
msgid "Choose [New List] from the 'Add to:' pull down menu"
msgstr "اختر [قائمة جديدة] من 'إضافة إلى: ' بالسحب أسفل القائمة"

#: ../../source/lists.rst:64
msgid "|image871|"
msgstr ""

#: ../../source/lists.rst:66
msgid "Name the list and choose what type of list this is"
msgstr "اسم القائمة واختيار نوع القائمة هذه"

#: ../../source/lists.rst:68
msgid "A Private List is managed by you and can be seen only by you"
msgstr "يتم إدارة القائمة الخاصة من قبلك، والإطلاع عليها فقط من قبلك"

#: ../../source/lists.rst:70
msgid "A Public List can be seen by everybody, but managed only by you"
msgstr ""
"ويمكن الاطلاع على قائمة العامة من قبل الجميع، ولكن يتم إدارتها فقط من قبلك "

#: ../../source/lists.rst:72
msgid ""
"Once the list is saved it will accessible from the Lists page and from the "
"'Add to' menu at the top of the search results."
msgstr ""
"عندما يتم حفظ القائمة سيكون من الممكن الوصول إليها من صفحة القوائم ، ومن "
"قائمة 'أضف إلى' في أعلى نتائج البحث."

#: ../../source/lists.rst:78
msgid "Add to a List"
msgstr "أضف إلى القائمة"

#: ../../source/lists.rst:80
msgid ""
"To add titles to an existing list click on the list name from the page of "
"lists"
msgstr "لإضافة عناوين إلى القائمة الموجودة على اسم القائمة من صفحة قوائم"

#: ../../source/lists.rst:83
msgid "|image872|"
msgstr ""

#: ../../source/lists.rst:85
#, fuzzy
msgid "To open a list you can click the list name."
msgstr "لإنشاء قائمة جديدة بالمستفيدين قم بالنقر على زر \"قائمة مستفيد جديدة\""

#: ../../source/lists.rst:87
#, fuzzy
msgid ""
"From that page you can add titles by scanning barcodes into the box at the "
"bottom of the page"
msgstr ""
"من صفحة القائمة تستطيع إضافة العناوين من خلال مسح باركوداتها إلى المربع أسفل "
"الصفحة."

#: ../../source/lists.rst:90
msgid "|image873|"
msgstr ""

#: ../../source/lists.rst:92
msgid ""
"A title can also be added to a list by selecting titles on the search "
"results page and choosing the list from the 'Add to' menu"
msgstr ""
"يمكن إضافة العنوان أيضا من خلال اختيار عناوين من صفحة نتائج البحث ومن خلال "
"اختيار القائمة من 'أضف إلى ' القائمة"

#: ../../source/lists.rst:95
msgid "|image874|"
msgstr ""

#: ../../source/lists.rst:100
msgid "Viewing Lists"
msgstr "عرض القوائم"

#: ../../source/lists.rst:102
msgid "To see the contents of a list, visit the Lists page on the staff client"
msgstr "لمعرفة محتويات القائمة، اذهب إلى صفحة القوائم على العميل الموظف"

#: ../../source/lists.rst:104
msgid "|image875|"
msgstr ""

#: ../../source/lists.rst:106
msgid "Clicking on the 'List Name' will show the contents of the list"
msgstr "النقر على 'اسم قائمة \"سوف يتم إظهار محتويات القائمة"

#: ../../source/lists.rst:108
msgid "|image876|"
msgstr ""

#: ../../source/lists.rst:110 ../../source/lists.rst:169
msgid "From this list of items you can perform several actions"
msgstr "من هذه القائمة من المواد يمكنك تنفيذ العديد من الإجراءات"

#: ../../source/lists.rst:112
#, fuzzy
msgid "'New list' will allow you to create another list"
msgstr ""
"بالنقر على 'التحرير في المُضيف' سيسمح لك بتحرير المادة في التسجيلة المُضيفة."

#: ../../source/lists.rst:114
msgid ""
"'Edit' will allow you to edit the description and permissions for this list"
msgstr ""
"بالنقر على 'التحرير في المُضيف' سيسمح لك بتحرير المادة في التسجيلة المُضيفة."

#: ../../source/lists.rst:117
msgid ""
"'Send list' will send the list to the email address you enter (:ref:`view "
"sample List email <example-email-from-list-label>`)"
msgstr ""

#: ../../source/lists.rst:120
msgid ""
"'Download list' will allow you to download the cart using one of 3 default "
"formats or your :ref:`CSV Profiles`"
msgstr ""

#: ../../source/lists.rst:123
msgid "'Print list' will present you with a printable version of the list"
msgstr "'طباعة' سوف تقدم لك مع نسخة قابلة للطباعة من السلة"

#: ../../source/lists.rst:125
msgid ""
"Using the filters at the top of each column you can find specific items in "
"your list."
msgstr ""

#: ../../source/lists.rst:131
msgid "Merging Bibliographic Records Via Lists"
msgstr "دمج التسجيلات الببليوغرافية من خلال القوائم"

#: ../../source/lists.rst:133
#, fuzzy
msgid ""
"One way to merge together duplicate bibliographic records is to add them to "
"a list and use the Merge Tool from there."
msgstr ""
"أسهل طريقة لدمج التسجيلات الببليوغرافية المكررة مع بعضها هي إضافتهم إلى "
"القائمة واستخدام ادوات الدمج من هنا."

#: ../../source/lists.rst:136
msgid "|image877|"
msgstr ""

#: ../../source/lists.rst:138
msgid ""
"Once you have selected the records to merge together the process is the same "
"as if you had chosen to :ref:`merge via cataloging <merging-records-label>`."
msgstr ""

#: ../../source/lists.rst:144
msgid "Cart"
msgstr "سلة"

#: ../../source/lists.rst:146
msgid ""
"The cart is a temporary holding place for items in the OPAC and/or staff "
"client. The cart will be emptied once the session is ended (by closing the "
"browser or logging out). The cart is best used for performing batch "
"operations (holds, printing, emailing) or for getting a list of items to be "
"printed or emailed to yourself or a patron."
msgstr ""
"السلة هي مكان التخزين المؤقت للمواد في الأوباك و/ أو عميل الموظفين. سيتم "
"إفراغ السلة بمجرد إنهاء الجلسة (عن طريق إغلاق المتصفح أو تسجيل الخروج). "
"السلة هي أفضل استخدام  لتنفيذ عمليات الدفعة (الحجز والطباعة والإرسال عبر "
"البريد الإلكتروني) أو للحصول على قائمة من المواد التي سيتم طباعتها أو "
"إرسالها عبر البريد الالكتروني لنفسك أو للمستفيد."

#: ../../source/lists.rst:152
msgid ""
"If you would like to enable the cart in the staff client, you need to set "
"the :ref:`intranetbookbag` system preference to 'Show.' To add things to the "
"cart, search the catalog and select the items you would like added to your "
"cart and choose 'Cart' from the 'Add to' menu"
msgstr ""

#: ../../source/lists.rst:158
msgid "|image878|"
msgstr ""

#: ../../source/lists.rst:160
msgid ""
"A confirmation will appear below the cart button at the top of the staff "
"client"
msgstr "التأكيد سيظهر أسفل زر السلة في الجزء العلوي من عميل الموظفين"

#: ../../source/lists.rst:163
msgid "|image879|"
msgstr ""

#: ../../source/lists.rst:165
msgid ""
"Clicking on the Cart icon will provide you with the contents of the cart"
msgstr "النقرعلى أيقونة السلة سوف توفر لك مع المحتويات من السلة"

#: ../../source/lists.rst:167
msgid "|image880|"
msgstr ""

#: ../../source/lists.rst:171
msgid "'More details' will show more information about the items in the cart"
msgstr ""

#: ../../source/lists.rst:173
msgid ""
"'Send' will send the list to the email address you enter (:ref:`view sample "
"Cart email <example-email-from-cart-label>`)"
msgstr ""

#: ../../source/lists.rst:176
msgid ""
"'Download' will allow you to download the cart using one of 3 default "
"formats or your :ref:`CSV Profiles`"
msgstr ""

#: ../../source/lists.rst:179
msgid "'Print' will present you with a printable version of the cart"
msgstr "'طباعة' سوف تقدم لك مع نسخة قابلة للطباعة من السلة"

#: ../../source/lists.rst:181
msgid "'Empty and Close' will empty the list and close the window"
msgstr "'أفرغ وأغلق' سيتم إفراغ القائمة وإغلاق النافذة"

#: ../../source/lists.rst:183
msgid "'Hide Window' will close the window"
msgstr "'إخفاء نافذة' سوف يتم إغلاق النافذة"
